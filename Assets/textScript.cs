﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class textScript : MonoBehaviour {

    public string text;
    public Text[] textField;
    private bool textShown;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    void removeText()
    {
        foreach (Text t in textField)
        {
            t.text = "";
        }
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if(coll.gameObject.tag == "Player" && !textShown)
        {
            foreach(Text t in textField)
            {
                t.text = text;
            }
            textShown = true;
            Invoke("removeText", 4f);
        }
    }

}
