﻿using UnityEngine;
using System.Collections;

public class anvilScript : MonoBehaviour {

    public GameObject anvil;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    void OnTriggerEnter2D(Collider2D coll)
    {
        if(coll.gameObject.tag == "Player")
        {
            anvil.GetComponent<Rigidbody2D>().gravityScale = 2f;
            anvil.GetComponent<SpriteRenderer>().enabled = true;
            GetComponent<BoxCollider2D>().enabled = false;
        }
    }
}
