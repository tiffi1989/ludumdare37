﻿using UnityEngine;
using System.Collections;

public class SkelettScript : MonoBehaviour {

    private bool looksLeft, hasAxe = true;
    public GameObject axeLeft, axeRight;
    private bool throwing, getsANewAxe;
    private Rigidbody2D rigid;
    public float speed;
    private Animator ani;
    public BoxCollider2D area;
    private GameObject player;
    private BoxCollider2D playerColl;
    public float throwSpeed;
    public GameObject axeLeftPrefab, axeRightPrefab;
    public GameObject leftArm, rightArm;
    private float timeSinceLastThrow;
    private bool isInZone = true;


	// Use this for initialization
	void Start () {
        this.rigid = GetComponent<Rigidbody2D>();
        this.ani = GetComponent<Animator>();
        this.player = GameObject.Find("Player");
        this.playerColl = this.player.GetComponent<BoxCollider2D>();


    }
	
	// Update is called once per frame
	void Update () {
        timeSinceLastThrow += Time.deltaTime;

        if (!throwing && area.IsTouching(playerColl) && hasAxe && timeSinceLastThrow > 5 && isInZone)
        {
            timeSinceLastThrow = 0;
            throwing = true;
            if(player.transform.position.x < transform.position.x)
            {
                looksLeft = true;
                this.ani.SetBool("WalkingLeft", true);
                ani.SetTrigger("ThrowLeft");
                Invoke("throwAxe", .5f);
            }
            else
            {
                looksLeft = false;
                this.ani.SetBool("WalkingLeft", false);
                ani.SetTrigger("ThrowRight");
                Invoke("throwAxe", .5f);
            }
        }       

        if ((!throwing && hasAxe) || getsANewAxe)
        {
            this.rigid.velocity = looksLeft ? (new Vector2(-1, this.rigid.velocity.y) * speed * Time.deltaTime) : (new Vector2(1, this.rigid.velocity.y) * speed * Time.deltaTime);
        }
	
	}

    void throwAxe()
    {
        GameObject axe = looksLeft ? axeLeft : axeRight;
        if (looksLeft)
            axeRight.GetComponent<SpriteRenderer>().enabled = false;
        else
            axeLeft.GetComponent<SpriteRenderer>().enabled = false;
        Rigidbody2D axeBody = axe.GetComponent<Rigidbody2D>();
        axe.transform.parent = null;
        axeBody.isKinematic = false;
        axeBody.AddTorque(5000);
        axeBody.AddForce(new Vector2(10 * throwSpeed * (looksLeft ? -1 : 1), .5f * throwSpeed));
        axe.GetComponent<CircleCollider2D>().enabled = true;

        Invoke("throwingDone", 2.5f);

    }

    void throwingDone()
    {
        looksLeft = false;
        this.ani.SetBool("WalkingLeft", false);
        ani.SetTrigger("ThrowingDone");
        getsANewAxe = true;
        throwing = false;
        hasAxe = false;
    }

    void getNewAxe()
    {

        GameObject axe = axeRight;
        axe.GetComponent<SpriteRenderer>().enabled = false;
        axe.transform.parent = rightArm.transform ;
        axe.GetComponent<Rigidbody2D>().isKinematic = true;
        axe.GetComponent<CircleCollider2D>().enabled = false;

        axe.transform.localPosition = new Vector3(-0.2864968f, -6.138956f, 0);
        axe.transform.localRotation = Quaternion.Euler(0, 0, -133.639f);
        axe.transform.localScale = new Vector3(1.111111f, 1.111111f, 1);

        axeLeft.GetComponent<SpriteRenderer>().enabled = false;
        axeLeft.transform.parent = leftArm.transform;
        axeLeft.GetComponent<Rigidbody2D>().isKinematic = true;
        axeLeft.GetComponent<CircleCollider2D>().enabled = false;

        axeLeft.transform.localPosition = new Vector3(0.1874724f, -5.584919f, 0);
        axeLeft.transform.localRotation = Quaternion.Euler(0, 0, 129.67f);
        axeLeft.transform.localScale = new Vector3(1, 1, 1);
        getsANewAxe = false;
        ani.SetTrigger("GetANewAxe");

        Invoke("activateNewAxe", .4f);

    }

    void activateNewAxe()
    {
        axeRight.GetComponent<SpriteRenderer>().enabled = true;

        Invoke("turnRightAfterNewAxe", .7f);
    }

    void turnRightAfterNewAxe()
    {
        looksLeft = true;
        hasAxe = true;
        ani.SetTrigger("GotANewAxe");
        this.ani.SetBool("WalkingLeft", true);
        axeLeft.GetComponent<SpriteRenderer>().enabled = true;
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "SkelettArea")
        {
            isInZone = true;
        }
    }



    void OnTriggerExit2D(Collider2D coll)
    {
        if(coll.gameObject.tag == "SkelettArea")
        {

            isInZone = false;
            this.rigid.velocity = new Vector2();
            if (!hasAxe)
            {
                getNewAxe();
                return;
            }
            if (looksLeft)
            {
                looksLeft = false;
                this.ani.SetBool("WalkingLeft", false);
            }
            else
            {
                looksLeft = true;
                this.ani.SetBool("WalkingLeft", true);
            }
        }
    }    
}
