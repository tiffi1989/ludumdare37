﻿using UnityEngine;
using System.Collections;

public class ChainScript : MonoBehaviour {

    HingeJoint2D joint;

	// Use this for initialization
	void Start () {
        this.joint = GetComponent<HingeJoint2D>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnCollisionEnter2D(Collision2D coll)
    {
        if(coll.gameObject.tag == "Bullet" && joint != null)
        {
            joint.breakForce = 0;
        }
    }
}
