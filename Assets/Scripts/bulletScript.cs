﻿using UnityEngine;
using System.Collections;

public class bulletScript : MonoBehaviour {

    public GameObject player;

	// Use this for initialization
	void Start () {
        this.player = GameObject.Find("Player");
	}
	
	// Update is called once per frame
	void Update () {
        if (player.transform.position.x - transform.position.x > 50  || player.transform.position.x - transform.position.x < -50)
        {
            destroyMe();
        }
        
	
	}

    void OnCollisionEnter2D(Collision2D coll)
    {
        Invoke("destroyMe", .01f);
    }

    void destroyMe()
    {
        Destroy(gameObject);
    }
}
