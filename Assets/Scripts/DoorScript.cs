﻿using UnityEngine;
using System.Collections;

public class DoorScript : MonoBehaviour {

    public GameObject door;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D coll)
    {
        if(coll.gameObject.tag == "Player") {
            door.GetComponent<Rigidbody2D>().gravityScale = .15f;
            coll.gameObject.GetComponent<PlayerController>().looseHat();
            gameObject.GetComponent<BoxCollider2D>().enabled = false;
        }
    }
}
