﻿using UnityEngine;
using System.Collections;

public class rockScript : MonoBehaviour {

    Rigidbody2D rig;
    public bool rolling;
    private Vector3 startPoint;
    public Vector3 differentStartPoint;
    public int depth;

	// Use this for initialization
	void Start () {
        rig = GetComponent<Rigidbody2D>();
        if (rolling)
        {
            if (differentStartPoint == null || differentStartPoint == new Vector3(0,0,0))
                startPoint = transform.position;
            else
                startPoint = differentStartPoint;
        }
	}
	
	// Update is called once per frame
	void Update () {
	    if(transform.position.y < -depth)
        {
            if (rolling) {
                transform.position = startPoint;
            } else {
                transform.position = new Vector3(transform.position.x, 35, transform.position.z);
            }
            rig.velocity = new Vector2();
        }
    }
}
