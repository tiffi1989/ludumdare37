﻿using UnityEngine;
using System.Collections;

public class anvilTriggerScript : MonoBehaviour {

    private bool started;
    public GameObject anvil;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Player" && !started)
        {
            Debug.Log("Anvil");
            Instantiate(anvil, new Vector3(transform.position.x, transform.position.y + 15, 0), Quaternion.identity);
            started = true;
        }

    }
}
