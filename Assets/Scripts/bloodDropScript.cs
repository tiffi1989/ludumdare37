﻿using UnityEngine;
using System.Collections;

public class bloodDropScript : MonoBehaviour {

    private Rigidbody2D rig;
    private GameObject player;

	// Use this for initialization
	void Start () {
        rig = GetComponent<Rigidbody2D>();
        player = GameObject.Find("Player");
	}
	
	// Update is called once per frame
	void Update () {
        Vector2 dir = rig.velocity;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg + 90;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }
}
