﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

    public Camera mainCam;
    private Rigidbody2D rigid;
    public float speed;
    public float jumpForce;
    public GameObject hat;
    public GameObject bullet;
    public float fireSpeed;
    public float bulletSpeed;
    private float fireTimeCounter;
    public GameObject playerHead;
    public Sprite sadHead, lastHead;
    public Sprite happyHead;
    private bool hatOn = true;
    private float cryTimer;
    public GameObject tear;
    private bool right = true;
    private bool dead, hitByAnvil, grounded, hasMinigun;
    public GameObject blood;
    public GameObject headDead;
    public GameObject miniGun;
    private Animator miniAni;
    public float miniGunLoadSpeed;
    public Light miniLight;
    public SpriteRenderer gunFire;
    public Animator ani;
    private bool gameStarted, introRunning, outroRunning;
    public Text[] topBar, bigMiddle;
    private int bigMiddleCounter;
    private string[] middleTexts = { "3", "2", "1", "GO", "" };

	void Start () {
        this.rigid = GetComponent<Rigidbody2D>();
        this.miniAni = miniGun.GetComponent<Animator>();
        this.ani = GetComponent<Animator>();
        miniAni.speed = 0;
        if (ApplicationModel.savepoint == 1)
            this.transform.position = new Vector3(302.4f, -44.9f, 0);
        this.mainCam.transform.position = new Vector3(transform.position.x, transform.position.y, -10);
        this.mainCam.orthographicSize = 3;
        ani.SetTrigger("begin");

    }



    void FixedUpdate () {

        if(!gameStarted && Input.GetAxis("Jump") > 0 && !introRunning)
        {
            ani.SetTrigger("throwHat");
            introRunning = true;
            startGame();
        }

        if(introRunning && this.mainCam.orthographicSize < 10.5f && !outroRunning)
        {
            Debug.Log("dd");

            float curPost = this.mainCam.transform.position.y;
            if (curPost < transform.position.y + 3)
                this.mainCam.transform.position = new Vector3(transform.position.x, curPost + .02f, -10);
            this.mainCam.orthographicSize += .03f;
        }

        if (outroRunning)
        {
            if (this.mainCam.orthographicSize > 3f)
            {
                float curPost = this.mainCam.transform.position.y;
                if (curPost > transform.position.y)
                {
                    this.mainCam.transform.position = new Vector3(transform.position.x, curPost - .02f, -10);
                }
                this.mainCam.orthographicSize -= .03f;

            }


            return;
        }


        if (dead || !gameStarted)
            return;
        Vector2 position = transform.position;
        float movement = Input.GetAxis("Horizontal");
        if (position.y > -80)
            this.mainCam.transform.position = new Vector3(position.x, position.y +3, -10);
        else
            die(false);        
        if(movement != 0)
        {
            
            rigid.velocity = new Vector2(movement * (hatOn ? speed : speed/1.5f),rigid.velocity.y);
            this.ani.SetBool("isWalking", true);
        }

        if(rigid.velocity.x == 0)
        {
            this.ani.SetBool("isWalking", false);

        }

        if (movement < 0 )
        {
            transform.localScale = new Vector3(-0.4f, 0.4f, 0.4f);
            right = false;
        }
        if(movement > 0)
        {
            transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
            right = true;
        }

        if(Input.GetAxis("Jump") != 0 && grounded)
        {
            rigid.velocity = new Vector2(rigid.velocity.x, jumpForce);
            rigid.AddTorque(0);
            grounded = false;
        }

        if (Input.GetAxis("Submit") != 0 && hatOn)
        {
            looseHat();
        }
        cryTimer += Time.deltaTime;
        if (!hatOn && cryTimer > .5f)
        {
            Instantiate(tear, new Vector3(transform.position.x + (right ? .5f : -.5f), transform.position.y - .1f, 0), Quaternion.identity);
            cryTimer = 0;
        }

        fireTimeCounter += Time.deltaTime;
        
        if(Input.GetAxis("Fire1") != 0 && fireTimeCounter > 1/ fireSpeed && hasMinigun)
        {
            if (miniAni.speed <= .9f)
                miniAni.speed += miniGunLoadSpeed ;
            else
            {
                fireTimeCounter = 0;
                GameObject bul = (GameObject)Instantiate(bullet, new Vector3(transform.position.x + (right ? 4 : -4), transform.position.y - 1, 0), Quaternion.identity);
                bul.GetComponent<Rigidbody2D>().velocity = (right ? Vector2.right : Vector2.left) * bulletSpeed;
                miniLight.enabled = true;
                gunFire.enabled = true;
                if (right)
                    rigid.AddForce(Vector2.left * 100);
                else
                    rigid.AddForce(Vector2.right * 100);
                Invoke("miniGunLightOff", .05f);
            }            
        }else
        {
            if(hasMinigun && miniAni.speed > 0)
            {
                miniGun.GetComponent<Animator>().speed -= miniGunLoadSpeed;
            }
        }
    }

    void startGame()
    {
        setTopText("");
        setNextMiddleText();
        Invoke("setNextMiddleText", 1f);
        Invoke("setNextMiddleText", 2f);
        Invoke("setNextMiddleText", 3f);
        Invoke("setNextMiddleText", 4f);

    }

    void setTopText(string text)
    {
        foreach (Text t in topBar)
        {
            t.text = text;
        }
    }

    void setNextMiddleText() {

        if (bigMiddleCounter == 3)
        {
            ani.SetTrigger("startGame");
        }
        if(bigMiddleCounter == 4)
            gameStarted = true;


        foreach (Text t in bigMiddle)
        {
            t.text = middleTexts[bigMiddleCounter];
        }
        bigMiddleCounter++;
    }

    void setMiddleText(string text)
    {
        foreach(Text t in bigMiddle)
        {
            t.text = text;
        }
    }

    void miniGunLightOff()
    {
        miniLight.enabled = false;
        gunFire.enabled = false;
    }

    public void looseHat()
    {

        hat.transform.parent = new GameObject().transform;

        //ani.SetBool("hasHat", false);
        ani.Rebind();
        CircleCollider2D col = hat.GetComponent<CircleCollider2D>();
        col.enabled = true;
        Rigidbody2D rig = hat.GetComponent<Rigidbody2D>();
        rig.isKinematic = false;
        rig.velocity = rigid.velocity * .5f;
        rig.AddTorque(20);
        hatOn = false;
        playerHead.GetComponent<SpriteRenderer>().sprite = sadHead;
        hat.GetComponentInChildren<Light>().color = Color.red;
        
    }

    void looseHatAtENd()
    {
        hat.transform.parent = new GameObject().transform;

        //ani.SetBool("hasHat", false);
        ani.Rebind();
        CircleCollider2D col = hat.GetComponent<CircleCollider2D>();
        col.enabled = true;
        Rigidbody2D rig = hat.GetComponent<Rigidbody2D>();
        rig.isKinematic = false;
        rig.velocity = rigid.velocity * .5f;
        rig.AddTorque(20);
        hat.GetComponentInChildren<Light>().color = Color.red;
        rig.AddForce(new Vector2(-500f, 500f));
    }

    void bloodBath()
    {
        die(true);

        GameObject.Find("bloodBath").GetComponent<SpriteRenderer>().enabled = true;
    }

    void die(bool noHead)
    {
        if (hatOn)
        {
            looseHat();
            hat.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 1) * 800);
        }
        hat.GetComponent<Rigidbody2D>().gravityScale = .3f;
        GetComponent<BoxCollider2D>().enabled = false;
        rigid.isKinematic = true;
        dead = true;
        SpriteRenderer[] renderers = GetComponentsInChildren<SpriteRenderer>();
        foreach (SpriteRenderer r in renderers)
        {
            r.enabled = false;
        }

        for (int i = 0; i < 20; i++)
        {
            GameObject b = (GameObject)Instantiate(blood, new Vector3(transform.position.x + (right ? .5f : -.5f), transform.position.y - .1f, 0), Quaternion.identity);
            Vector2 v;
            if (i % 2 == 0)
                v = new Vector2(Random.Range(-1f, 1f), Random.Range(.3f, .35f));
            else
                v = new Vector2(Random.Range(-1f, 1f), Random.Range(.65f, .7f));
            b.GetComponent<Rigidbody2D>().AddForce(v.normalized * 500 * Random.Range(.4f, .8f));
        }

        if(!noHead)
            Instantiate(headDead, new Vector3(transform.position.x + (right ? .5f : -.5f), transform.position.y - .1f, 0), Quaternion.identity);

        Invoke("loadLevel", 2f);
    }

    void loadLevel()
    {
        SceneManager.LoadScene("Main");
    }

    public void endGame()
    {
        outroRunning = true;
        rigid.velocity = new Vector2(0, rigid.velocity.y);
        playerHead.GetComponent<SpriteRenderer>().sprite = lastHead;
        looseHatAtENd();
        endGo();
    }

    void endGo()
    {
        ani.SetTrigger("endGame");
        setTopText("Hell yeah.. Im fabulous!!!");
        setMiddleText("YOU WIN");
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        Debug.Log(coll.gameObject.tag + " " + coll.collider.name);
        if (coll.gameObject.tag == "Floor" || coll.gameObject.tag == "Anvil") ;
            grounded = true;

        if(coll.gameObject.tag == "Anvil" || coll.collider.name == "ButtonOfTheDoor")
            hitByAnvil = true;       

        if (hitByAnvil && grounded)
            bloodBath();

        if (coll.gameObject.tag == "Spikeball")
            die(false);

        if(coll.gameObject.tag == "Hat")
        {            
            Debug.Log("Col" + coll.gameObject.tag);
            CircleCollider2D col = hat.GetComponent<CircleCollider2D>();
            col.enabled = false;
            Rigidbody2D rig = hat.GetComponent<Rigidbody2D>();
            rig.isKinematic = true;
            rig.velocity = new Vector2();
            hat.transform.parent = gameObject.transform;
            hat.transform.rotation = new Quaternion(0,0,0,0);     
            hat.transform.localPosition = new Vector3(-0.23f, 1.76f, 4);
            hat.layer = 9;
            playerHead.GetComponent<SpriteRenderer>().sprite = happyHead;
            ani.Rebind();
            hatOn = true;
            hat.GetComponentInChildren<Light>().color = new Color(1,.9f,.45f,1);
        }

        if (coll.gameObject.tag == "Axe")
        {
            if (hatOn)
            {
                looseHat();
                hat.GetComponent<Rigidbody2D>().AddForce(Vector2.up * 1000f);
            }
            else
                die(false);
        }


        if (coll.collider.name == "minigunOnAltar" && !hasMinigun)
        {
            Debug.Log("MiniGunOn");
            ApplicationModel.savepoint = 1;
            ani.Rebind();
            Destroy(coll.gameObject);
            hasMinigun = true;
            this.miniGun.GetComponent<SpriteRenderer>().enabled = true;
            Debug.Log(this.miniGun.GetComponent<SpriteRenderer>());
        }
    }
}
