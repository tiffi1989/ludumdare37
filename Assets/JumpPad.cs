﻿using UnityEngine;
using System.Collections;

public class JumpPad : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnCollisionEnter2D(Collision2D coll)
    {
        if(coll.gameObject.tag == "Player")
        {
            coll.gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.up * 3000f);
        }
    }
}
